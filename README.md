# 20.1 - URL Shortener

## Subject

Implement an URL shortener web service, with the following specification:

-   **Create** a new short URL:

    ```
    POST /url
    ```

    -   body: `url=<url>` (and optionally `short=<short-token>`, random if not given)
    -   returns: [201 Created](https://developer.mozilla.org/en-US/docs/Web/HTTP/Status/201) with `Location=/url/<short-token>` header

-   **Load** a short URL:

    ```
    GET /<short-token>
    ```

    -   returns on success: [302 Found](https://developer.mozilla.org/en-US/docs/Web/HTTP/Status/302) with `Location=<url>` header
    -   returns on error: 404 Not found

-   **Get information** about a short URL:

    ```
    GET /url/<short-token>
    ```

    -   returns on success: 200 OK with `Location=<url>` header
    -   returns on error: 404 Not found

-   **Update** a short URL:

    ```
    PUT /url/<short-token>
    ```

    -   body: `url=<url>`
    -   returns on success: [204 No content](https://developer.mozilla.org/en-US/docs/Web/HTTP/Status/204) with `Location=/url/<short-token>` header
    -   returns on error: 404 Not found

-   **Delete** a short URL:

    ```
    DELETE /url/<short-token>
    ```

    -   returns on success: [204 No content](https://developer.mozilla.org/en-US/docs/Web/HTTP/Status/204)
    -   returns on error: 404 Not found

### Implementation notes

-   Get inspiration from the given `/hello/<name>` route (handler and test), but once you have corrected the test, keep the `handlers/hello*.go` files unchanged and create your own.

-   Use a non-persistent, **in-memory** storage during development ([`sync.Map`](https://golang.org/pkg/sync/#Map)?) and inject it in your handlers as an **interface** (for mocking in the tests).

-   Keep the REST architecture as it is specified, but feel free to change the data format (JSON data, for example), or discuss the returned codes, headers and contents.

## Running and testing

[Go](https://golang.org/dl/) version 1.12 or above is required.

Run with `go run shortener`, or compile with `go build` (which produces a `./shortener` executable file).

Test server manually with either

```sh
http POST :8000/hello/world
curl -i -X POST http://localhost:8000/hello/world
```

Run automatic tests with `go test shortener/...`
