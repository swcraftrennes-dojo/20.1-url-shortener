package main

import (
	"fmt"
	"net/http"
	"shortener/handlers"
)

func main() {
	// setup routes
	router := http.NewServeMux()
	helloHandler := &handlers.Hello{}

	router.Handle("/hello/", helloHandler) // try this route with `http :8000/hello/world`

	// start HTTP server
	url := ":8000"
	fmt.Printf("Server listening on %s…\n", url)
	panic(http.ListenAndServe(url, router))
}
