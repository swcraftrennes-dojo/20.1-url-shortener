package handlers

import (
	"io/ioutil"
	"net/http"
	"net/http/httptest"
	"net/url"
	"strings"
	"testing"
)

func TestHello(t *testing.T) {
	// setup
	params := make(url.Values)
	params.Set("test", "toto")
	req := httptest.NewRequest(
		/* method */ http.MethodGet,
		/* path   */ "/hello/world",
		/* body   */ strings.NewReader(params.Encode()), // nil could be used instead of params (body not read by handler), they are here for demonstration purposes
	)
	rw := httptest.NewRecorder()
	handler := &Hello{}

	// execute
	handler.ServeHTTP(rw, req)
	response := rw.Result()

	// check
	// - metadata
	if response.StatusCode != http.StatusOK {
		t.Fatalf("HTTP error, received status %d", response.StatusCode)
	}
	contentType := getContentType(response)
	if contentType != "text/plain" {
		t.Fatalf("Unexpected content type %s, expected text/plain", contentType)
	}
	// - content
	body, err := ioutil.ReadAll(response.Body)
	if err != nil {
		t.Fatalf("Could not read response content: %v", err)
	}
	expected := "Hello world"
	actual := string(body)
	if actual != expected {
		t.Errorf("Expected content \"%s\", got \"%s\"", expected, actual)
	}
}
