package handlers

import (
	"net/http"
	"strings"
)

// Hello encapsulates handler dependencies
type Hello struct{}

// ServeHTTP is called by the server when the route /hello/<name> is hit
// (with any HTTP method).
func (h *Hello) ServeHTTP(w http.ResponseWriter, req *http.Request) {
	// extract "name" from request path
	name := getResourceIDFromPath(req)
	name = strings.TrimSpace(name)
	if name == "" {
		// return error (with custom status and content)
		http.Error(w, "Missing name in route", http.StatusBadRequest)
		return
	}
	// return response (with implicit 200 OK status)
	w.Write([]byte("Hello " + name + "!"))
}
