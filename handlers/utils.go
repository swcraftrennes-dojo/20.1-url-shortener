package handlers

import (
	"net/http"
	"strings"
)

// getResourceIDFromPath assumes the request is of the REST form:
// METHOD /resourceType/resourceID (with optional subcommands following)
// and returns the resourceID (2nd element in the absolute path)
func getResourceIDFromPath(req *http.Request) string {
	path := normalizedPath(req)
	pathElements := strings.SplitN(path, "/", 4)
	if len(pathElements) < 3 {
		// no resource identifier
		return ""
	}
	return pathElements[2]
}

// normalizedPath returns the absolute request path (starting with an initial "/")
func normalizedPath(req *http.Request) string {
	path := req.URL.Path
	if len(path) == 0 || path[0] != '/' {
		// empty or relative path: add an initial "/"
		path = "/" + path
	}
	return path
}

// getContentType returns the Content-type of a response, trimming final attributes
// (following first ";")
func getContentType(resp *http.Response) string {
	header := resp.Header.Get("Content-type")
	return strings.SplitN(header, ";", 2)[0]
}
